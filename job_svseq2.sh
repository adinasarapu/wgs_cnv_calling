#!/bin/sh
echo "Start - `date`"
#$ -N SVSeq9
#$ -q all.q
##$ -l h_rt=220:00:00
#$ -pe smp 20
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
##########################
# Predicting CNV regions #
##########################

PROJ_DIR="$HOME/zwick_rare/geisert"
# BWAIndex, WholeGenomeFasta
REF_FILE="$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/BWAIndex/genome.fa"
MAP_DIR="$HOME/zwick_rare/geisert/BWA"
CHR_DIR="$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/Chromosomes"

# -chrom chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 ch\
# ./SVseq2 -r reference -b bam_file_list -c chr --o output_file[result.txt]
for i in 9; do # {6..9}; do
	SID="SL14857${i}"
	OUT_DIR="${PROJ_DIR}/svseq2/${SID}"

	if [ ! -d ${OUT_DIR} ]; then
		mkdir -p ${OUT_DIR}
	fi
	for j in {18..22} X Y; do
		# Calling deletions
		${HOME}/SVseq2_2 \
			-r ${REF_FILE} \
			-b ${MAP_DIR}/${SID}/${SID}.dedupe.bam \
			-c chr${j} \
			--o ${OUT_DIR}/${SID}.svseq2.deletion.chr${j}.txt
		# Calling insertions
		${HOME}/SVseq2_2 \
			-insertion \
			-b ${MAP_DIR}/${SID}/${SID}.dedupe.bam \
			-c chr${j} \
			--o ${OUT_DIR}/${SID}.svseq2.insertion.chr${j}.txt
	done
done
