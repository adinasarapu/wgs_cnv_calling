#!/bin/sh

#$ -N SL156315
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 4
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

SID=SL156315

BAM_DIR=${HOME}/zwick_rare/TYEP0004/BWA/${SID}

module load picardtools/2.6.0
$PICARD AddOrReplaceReadGroups \
	I=${BAM_DIR}/${SID}.dedupe.bam \
	O=${BAM_DIR}/${SID}.rg.bam \
	RGID=None \
	RGLB=None \
	RGPL=illumina \
	RGPU=None \
	RGSM=${SID}
module load picardtools/2.6.0
