#!/bin/sh

echo "Start - `date`"
#$ -N Pindel
#$ -q all.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
##########################
# Predicting SV regions #
##########################

PROJ_DIR="$HOME/zwick_rare/geisert"
# BWAIndex, WholeGenomeFasta
REF_FILE="$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/BWAIndex/genome.fa"
MAP_DIR="$HOME/zwick_rare/geisert/BWA"
CHR_DIR="$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/Chromosomes"
OUT_DIR="${PROJ_DIR}/pindel"

if [ ! -d ${OUT_DIR} ]; then
	mkdir -p ${OUT_DIR}
fi
 
export TMPDIR=/tmp

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
	TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

# To generate sample file (bam_config.txt) use : pindel_config.sh

# Calling SV
pindel \
	-f ${REF_FILE} \
	-i ${PROJ_DIR}/pindel/bam_config.txt \
	-c ALL \
	-T 9 \
	-o ${TMP_DIR} 

# copy your local data to your user directory
rsync -av ${TMP_DIR}/ ${OUT_DIR}
	
# remove the temp directory
/bin/rm -fr ${TMP_DIR}
