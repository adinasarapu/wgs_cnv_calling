# Structural Variants (SV) including Copy-Number Variants (CNV) calling  


## Joint detection of copy number variations in parent-offspring trios  

STEP 1. git clone https://github.com/yongzhuang/TrioCNV.git  

STEP 2. Add local/missing jar files to Maven project (update pom.xml)  
Build maven project with propriatery libraries like RCaller-2.5.jar and igv.jar into TrioCNV/lib  
create a dir TrioCNV/local-maven-repo update pom.xml file  

<repositories>  
  <repository>  
   <id>local-maven-repo</id>  
   <url>file:///${project.basedir}/local-maven-repo</url>  
  </repository>   
 </repositories>  
 <dependencies>  
  <dependency>  
   <groupId>rcaller</groupId>  
   <artifactId>RCaller</artifactId>  
   <version>2.5</version>  
  </dependency>  
  <dependency>  
  <groupId>org.broad.igv</groupId>  
   <artifactId>igv</artifactId>  
   <version>2.3.23</version>  
  </dependency>  
 </dependencies>  

STEP 3. Enter "cd TrioCNV/" directory, run the following script, which  will add missing jars to your local maven project, `add_jar_to_mvn.sh`  

STEP 4. then run mvn build  
`mvn clean install -Dmaven.compiler.source=1.7 -Dmaven.compiler.target=1.7`  

STEP 5. To get BAM files use, `job_bwa_mpileup.sh`  

STEP 6. If necessary, update your BAM files  

Note: The Individual ID in Pedigree file must be same as the @RG SM tag of the BAM file. To add (or replace) @RG SM tag to BAM file run the following script `AddOrReplaceReadGroups.sh`  

STEP 7. Use the current file/script (HGCC, node02)

## CNV discovery and genotyping from depth-of-coverage by mapped reads

Install [ROOT](https://root.cern.ch) and [CNVnator](https://github.com/abyzovlab/CNVnator)  
Read mapping using `bwa mem` (use `job_bwa_mpileup.sh`)  

STEP 1: EXTRACTING READ MAPPING FROM BAM/SAM FILES  

STEP 2: CALCULATING STATISTICS  
This step must be completed before proceeding to partitioning and CNV calling.  

STEP 3: RD SIGNAL PARTITIONING  
Option -ngc specifies not to use GC corrected RD signal. Partitioning is the most time consuming step.  

STEP 4: CNV CALLING  

STEP 5: VISUALIZING SPECIFIED REGIONS  
`cnvnator -root file.root [-chrom chr_name1 ...] -view bin_size [-ngc]`  

# Insertion and deletion detection  

To call structural variations (SV) with Pindel use `job_pindel.sh`. To generate sample file (`bam_config.txt`) use : `pindel_config.sh`  
To call structural variations (SV) with SVseq2 use `job_svseq2.sh`
